#include "LoosePanel.h"


//Constructors
LoosePanel::LoosePanel(Score score)
	:m_score(score)
{
}


//Destructor
LoosePanel::~LoosePanel()
{
}


//Getters
Score LoosePanel::GetScore() const
{
	return m_score;
}


//Methods
std::ostream& operator<<(std::ostream& out, const LoosePanel& loose)
{
	out << "GAME OVER \n";
	out << "SCORE PLAYER: " << loose.m_score << "\n";
	out << "_____________________" << "\n";
	out << "TOTAL: " << loose.m_score << "\n";
	return out;
}


//void LoosePanel::DisplayGameOver()
//{
//	std::cout << "GAME OVER \n";
//	std::cout << "SCORE PLAYER: ";
//	std::cout << mScore << "\n";
//}
