#pragma once
#include <iostream>
#include <fstream>
#include "Rules.h"
#include"Timer.h"

// Constructor
Rules::Rules()
	:m_score(0)
	, m_hightScore(0)
	, m_title("Rules of the Game:\n")
	, m_rules("\n\n1.Press W/Up Arrow if you want to go forward\n2.Press A/Left Arrow if you want to go Left\n3.Press D/Right Arrow if you want to go Right\n4.Press S/Down Arrow if you downward\n5.Press Space if you want to shoot\n \nPress Enter to start a new game!\n\n                GOOD LUCK!")
{
}


// Destructor
Rules::~Rules()
{
}


//Methods
void Rules::PrintFirstStep()
{
	sf::RenderWindow window(sf::VideoMode(1191, 745), "Battle City");
	window.getSystemHandle();

	sf::Texture texture;
	if (!texture.loadFromFile("Image1.png"))
	{
		std::cout << "The Image1 does not load!" << std::endl;
	}

	//De mutat in .h
	sf::Sprite sprite;
	sprite.setTexture(texture);

	sf::Vector2i centerWindow((sf::VideoMode().width / 2) - 755, (sf::VideoMode::getDesktopMode().height / 2) - 390);
	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) || event.type == sf::Event::Closed)
			{
				window.close();
			}
		}
		window.clear();
		window.draw(sprite);
		window.display();
	}
}


void Rules::PrintSecondStep()
{
	sf::RenderWindow window(sf::VideoMode(1191, 745), "Rules of the game!");
	window.getSystemHandle();

	sf::Texture texture;
	if (!texture.loadFromFile("Image2.png"))
	{
		std::cout << "The Image2 does not load!" << std::endl;
	}

	sf::Sprite sprite;
	sprite.setTexture(texture);

	sf::Font font;
	if (!font.loadFromFile("Roboto-Black.ttf"))
	{
		std::cout << "Error loading the file";
	}

	sf::Text title(Rules::m_title, font, 100);
	title.setStyle(sf::Text::Italic);
	title.setFillColor(sf::Color::Red);
	title.setPosition(60, 200);
	title.setCharacterSize(40);

	sf::Text text(Rules::m_rules, font, 100);
	text.setStyle(sf::Text::Bold);
	text.setFillColor(sf::Color::Red);
	text.setPosition(75, 220);
	text.setCharacterSize(30);

	sf::Vector2i centerWindow((sf::VideoMode().width / 2) - 755, (sf::VideoMode::getDesktopMode().height / 2) - 390);
	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) || event.type == sf::Event::Closed)
			{
				window.close();
			}
		}
		window.clear();
		window.draw(sprite);
		window.draw(title);
		window.draw(text);
		window.display();

	}
}


void Rules::End() {
	sf::RenderWindow window(sf::VideoMode(1324, 745), "Game Over!");
	window.getSystemHandle();

	sf::Texture texture;
	if (!texture.loadFromFile("GameOverImage.png"))
	{
		std::cout << "The GameOverImage does not load!" << std::endl;
	}

	sf::Sprite sprite;
	sprite.setTexture(texture);

	sf::Font font;
	if (!font.loadFromFile("Roboto-Black.ttf"))
	{
		std::cout << "Error loading the file";
	}

	// Display Score
	sf::Text scoreTextName;
	scoreTextName.setFont(font);
	scoreTextName.setString("SCORE: ");
	scoreTextName.setCharacterSize(30);
	scoreTextName.setFillColor(sf::Color::Red);
	scoreTextName.setPosition(450.f, 485.f);
	scoreTextName.setStyle(sf::Text::Bold);

	sf::Text scoreText;
	scoreText.setFont(font);
	scoreText.setString(std::to_string(m_score));
	scoreText.setCharacterSize(30);
	scoreText.setFillColor(sf::Color::Red);
	scoreText.setPosition(580.f, 485.f);
	scoreText.setStyle(sf::Text::Bold);


	// Display HighScore
	sf::Text highScoreTextName;
	highScoreTextName.setFont(font);
	highScoreTextName.setString("HIGH SCORE: ");
	highScoreTextName.setCharacterSize(30);
	highScoreTextName.setFillColor(sf::Color::Red);
	highScoreTextName.setPosition(450.f, 520.f);
	highScoreTextName.setStyle(sf::Text::Bold);

	sf::Text highScoreText;
	highScoreText.setFont(font);
	highScoreText.setString(std::to_string(m_hightScore));
	highScoreText.setCharacterSize(30);
	highScoreText.setFillColor(sf::Color::Red);
	highScoreText.setPosition(660.f, 520.f);
	highScoreText.setStyle(sf::Text::Bold);


	sf::Vector2i centerWindow((sf::VideoMode().width / 2) - 755, (sf::VideoMode::getDesktopMode().height / 2) - 390);
	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) || event.type == sf::Event::Closed)
			{
				window.close();
			}
		}
		window.clear();

		// Draw Background
		window.draw(sprite);

		// Draw Score
		window.draw(scoreTextName);
		window.draw(scoreText);

		// Draw High Score
		window.draw(highScoreTextName);
		window.draw(highScoreText);

		window.display();
	}
}
void Rules::Win()
{
	sf::RenderWindow window(sf::VideoMode(1324, 745), "Congatulations!");
	window.getSystemHandle();

	sf::Texture texture;
	if (!texture.loadFromFile("WinnerGame.png"))
	{
		std::cout << "The WinnerGame does not load!" << std::endl;
	}

	sf::Sprite sprite;
	sprite.setTexture(texture);

	sf::Font font;
	if (!font.loadFromFile("Roboto-Black.ttf"))
	{
		std::cout << "Error loading the file";
	}


	// Display Score
	sf::Text scoreTextName;
	scoreTextName.setFont(font);
	scoreTextName.setString("SCORE: ");
	scoreTextName.setCharacterSize(30);
	scoreTextName.setFillColor(sf::Color::Red);
	scoreTextName.setPosition(450.f, 485.f);
	scoreTextName.setStyle(sf::Text::Bold);

	sf::Text scoreText;
	scoreText.setFont(font);
	scoreText.setString(std::to_string(m_score));
	scoreText.setCharacterSize(30);
	scoreText.setFillColor(sf::Color::Red);
	scoreText.setPosition(580.f, 485.f);
	scoreText.setStyle(sf::Text::Bold);


	// Display HighScore
	sf::Text highScoreTextName;
	highScoreTextName.setFont(font);
	highScoreTextName.setString("HIGH SCORE: ");
	highScoreTextName.setCharacterSize(30);
	highScoreTextName.setFillColor(sf::Color::Red);
	highScoreTextName.setPosition(450.f, 520.f);
	highScoreTextName.setStyle(sf::Text::Bold);

	sf::Text highScoreText;
	highScoreText.setFont(font);
	highScoreText.setString(std::to_string(m_hightScore));
	highScoreText.setCharacterSize(30);
	highScoreText.setFillColor(sf::Color::Red);
	highScoreText.setPosition(660.f, 520.f);
	highScoreText.setStyle(sf::Text::Bold);


	sf::Vector2i centerWindow((sf::VideoMode().width / 2) - 755, (sf::VideoMode::getDesktopMode().height / 2) - 390);
	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) || event.type == sf::Event::Closed)
			{
				window.close();
			}
		}
		window.clear();

		// Draw Background
		window.draw(sprite);

		// Draw Score
		window.draw(scoreTextName);
		window.draw(scoreText);

		// Draw High Score
		window.draw(highScoreTextName);
		window.draw(highScoreText);

		window.display();
	}
}

void Rules::ScoreUP()
{
	m_score++;
	if (m_hightScore < m_score)
		m_hightScore = m_score;
}

void Rules::ScoreDOWN()
{
	if (m_score > 0)
		m_score--;
}
