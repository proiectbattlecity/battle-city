#pragma once
#include<iostream>
#include<array>
#include"Wall.h"
#include"Tank.h"
#include"Block.h"
#include"Bullet.h"
#include <SFML/Graphics.hpp>
#include"Player.h"
#include"EnemyTank.h"
#include <vector>

class InGamePanel
{
public:
	//Constructors
	InGamePanel(Player& player, EnemyTank& enemyTank, EnemyTank& enemyTank1, EnemyTank& enemyTank2, EnemyTank& enemyTank3, sf::RenderWindow& window);

	//Destructor
	~InGamePanel();

};

