#pragma once
#include<iostream>

class Score
{
public:
	//Constructors
	Score();

	//Destructor
	~Score();

	//Getters
	uint16_t GetScore() const;

	//Setters
	void SetScore(const uint16_t& score);

public:
	uint16_t m_score;
};

