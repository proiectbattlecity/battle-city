#include "BrickWall.h"


//Constructors
BrickWall::BrickWall(const sf::Vector2f& position)
{
	sf::Texture texture;
	texture.loadFromFile("GravelSpecial.png");

	m_brick.setSize(sf::Vector2f(10, 10));
	m_brick.setTexture(&texture);
	m_brick.setPosition(position);
}


//Destructor
BrickWall::~BrickWall()
{
}


//Getters
uint8_t BrickWall::GetDimension() const
{
	return m_dimension;
}

float BrickWall::GetPositionOfX() const
{
	return m_positionOfX;
}

float BrickWall::GetPositionOfY() const
{
	return m_positionOfY;
}


//Setters
void BrickWall::SetDimension(const uint8_t& dimension)
{
	m_dimension = dimension;
}

void BrickWall::SetPositionOfX(const float& positionOfX)
{
	m_positionOfX = positionOfX;
}

void BrickWall::SetPositionOfY(const float& positionOfY)
{
	m_positionOfY = positionOfY;
}


//Methods
void BrickWall::Draw(sf::RenderWindow& window)
{
	window.draw(m_brick);
}
