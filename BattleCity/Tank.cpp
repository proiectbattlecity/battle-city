#include "Tank.h"

//Constructors
Tank::Tank()
	: m_lives(NULL)
	, m_color("NULL")
	, m_moves(NULL)
	, m_size(NULL)
	, m_positionOfX(NULL)
	, m_positionOfY(NULL)
{
}

Tank::Tank(int8_t m_lives, std::string m_color, float m_size, float m_moves, float position_x, float position_y)
	:m_lives(m_lives)
	, m_color(m_color)
	, m_size(m_size)
	, m_moves(m_moves)
	, m_positionOfX(position_x)
	, m_positionOfY(position_y)
{
}

Tank::Tank(const Tank& other)
{
	this->m_lives = other.m_lives;
	this->m_color = other.m_color;
	this->m_size = other.m_size;
	this->m_moves = other.m_moves;
	this->m_positionOfX = other.m_positionOfX;
	this->m_positionOfY = other.m_positionOfY;
}

Tank::Tank(Tank&& other)
{
	*this = std::move(other);
}

Tank* Tank::operator=(const Tank& other)
{
	this->m_color = other.m_color;
	this->m_lives = other.m_lives;
	this->m_size = other.m_size;
	this->m_moves = other.m_moves;
	this->m_positionOfX = other.m_positionOfX;
	this->m_positionOfY = other.m_positionOfY;
	return this;
}


//Destructor
Tank::~Tank()
{
}


//Getters
int8_t Tank::GetLives() const
{
	return m_lives;
}

std::string Tank::GetColor() const
{
	return m_color;
}

float Tank::GetSize() const
{
	return m_size;
}

float Tank::GetMoves() const
{
	return m_moves;
}

float Tank::GetPositionOfX() const
{
	return m_positionOfX;
}

float Tank::GetPositionOfY() const
{
	return m_positionOfY;
}


//Setters
void Tank::SetPositionOfX(const float& initialPositionOfX)
{
	this->m_positionOfX = initialPositionOfX;
}

void Tank::SetPositionOfY(const float& initialPositionOfY)
{
	this->m_positionOfY = initialPositionOfY;
}