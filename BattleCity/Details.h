#pragma once
#include "Timer.h"

class Details
{
public:
	//Constructors
	Details();
	Details(const Timer& time, const uint32_t& score, const uint16_t& lives);

	//Destructor
	~Details();

private:
	Timer m_time;
	uint32_t m_score;
	uint16_t m_lives;
};

