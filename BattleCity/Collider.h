#pragma once
#include <SFML/Graphics.hpp>
#include<iostream>
class Collider
{
public:
	//Constructors
	Collider( sf::RectangleShape& body);

	//Destructor
	~Collider();

	//Getters
	sf::Vector2f GetPosition() const;
	sf::Vector2f GetHalfSize() const;

	//Methods
	void Move(float dx, float dy);
	bool CheckCollision(Collider other, float push); //it will be 0 because we don't want to move the wall

private:
	sf::RectangleShape& m_body;
};

