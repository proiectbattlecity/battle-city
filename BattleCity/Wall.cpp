#include "Wall.h"
#include<iostream>
#include "Collider.h"


//Constructors
Wall::Wall()
{
	m_width = 0;
	m_height = 0;
	/*mMatrix = new BrickWall*[mHeight];
	for (auto i = 0; i < mHeight; i++)
		mMatrix[i] = new BrickWall[mWidth];*/
}

Wall::Wall(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position)
{
	m_wall.setSize(size);
	m_wall.setTexture(texture);
	//am mutat originea obiectului in mijloc
	m_wall.setOrigin(size / 2.0f);
	//am mutat obiectul sa apara in mijloc jos
	m_wall.setPosition(position);
}

Wall::Wall(uint32_t width, uint32_t height)
{
	m_width = width;
	m_height = height;
	/*mMatrix = new BrickWall*[mHeight];
	for (auto i = 0; i < mHeight; i++)
		mMatrix[i] = new BrickWall[mWidth];*/
}

Wall::Wall(const Wall& other)
{
	this->m_height = other.m_height;
	this->m_width = other.m_width;
	/*this->mMatrix = new BrickWall*[mHeight];
	for (auto i = 0; i < mHeight; i++)
		this->mMatrix[i] = new BrickWall[mWidth];*/
}

Wall& Wall::operator=(const Wall& other)
{
	if (this != &other) {
		if (other.m_height != m_height || other.m_width != m_width)
		{
			delete[] m_matrix;
			m_height = 0;
			m_width = 0;
			/*mMatrix = new BrickWall*[other.mHeight];
			for (auto i = 0; i < other.mHeight; i++)
				mMatrix[i] = new BrickWall[other.mWidth];*/
		}
		return *this;

	}
}

Wall::Wall(Wall&& other) noexcept
{
	//*this = std::move(other);
	m_width = other.m_width;
	m_height = other.m_height;
	//mMatrix = other.mMatrix;
	/*mMatrix = new BrickWall*[other.mHeight];
	for (auto i = 0; i < other.mHeight; i++)
		mMatrix[i] = new BrickWall[other.mWidth];*/
		//mMatrix = nullptr;
		/*for (auto i = 0; i < other.mHeight; i++)
			other.mMatrix[i] = nullptr;*/
			//other.mMatrix = nullptr;
	other.m_width = 0;
	other.m_height = 0;
}

Wall& Wall::operator=(Wall&& other) noexcept
{
	if (this == &other)
	{
		return *this;
	}
	/*for (auto i = 0; i < mHeight; i++)
		delete[] mMatrix[i];
	delete[] mMatrix;*/
	//mMatrix = other.mMatrix;
	/*mMatrix = new BrickWall*[other.mHeight];
	for (auto i = 0; i < other.mHeight; i++)
		mMatrix[i] = new BrickWall[other.mWidth];
	for (auto i = 0; i < other.mHeight; i++)
		other.mMatrix[i] = nullptr;*/
		//other.mMatrix = nullptr;
	other.m_width = 0;
	other.m_height = 0;
	return *this;
}


//Destructor
Wall::~Wall()
{
	/*for (auto i = 0; i < mHeight; i++)
		delete[] mMatrix[i];
	delete[] mMatrix;*/
}


//Getters
float Wall::GetPositionOfX() const
{
	return m_positionOfX;
}

float Wall::GetPositionOfY() const
{
	return m_positionOfY;
}

Collider Wall::GetCollider()
{
	return Collider(m_wall);
}


//Setters
void Wall::SetWidth(const uint32_t& width)
{
	m_width = width;
}

void Wall::SetHeight(const uint32_t& height)
{
	m_height = height;
}

void Wall::SetPositionOfX(const float& x)
{
	m_positionOfX = x;
}

void Wall::SetPositionOfY(const float& y)
{
	m_positionOfY = y;
}


//Vector of objects
Wall::Wall(int brickNumber, sf::Vector2f position)
{
	m_bricks.push_back(BrickWall(position));
	for (int i = 0; i < brickNumber * 10; i += 10)
	{
		m_bricks.push_back(BrickWall(sf::Vector2f(position.x + i, position.y)));
	}
}


//Methods
void Wall::Draw(sf::RenderWindow& window)
{
	window.draw(m_wall);
}

void Wall::DrawWallOfBricks(sf::RenderWindow& window)
{
	for (unsigned int i = 0; i < m_bricks.size(); i++)
	{
		m_bricks[i].Draw(window);
	}
}

void Wall::CheckBulletCollision(Projectile bullet)
{
	if (bullet.GetRight() > m_wall.getPosition().x&& bullet.GetTop() < m_wall.getPosition().y + m_wall.getSize().y && bullet.GetBottom() > m_wall.getPosition().y)
	{
		//mEnemyTank.setPosition(sf::Vector2f(4234432, 4234423));
		m_wall.setTexture(NULL);
		m_wall.setFillColor(sf::Color::Transparent);
	}
}