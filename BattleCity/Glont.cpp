#include "Glont.h"
Glont::Glont(sf::Texture* bulletTexture, sf::Vector2f size, sf::Vector2f position)
{
	//m_bullet.setOrigin(sf::Vector2f(25, 10));

	// Setez dimensiunea glontului
	m_bullet.setSize(sf::Vector2f(25, 10));
	// Setez originea glontului in mijloc
	m_bullet.setOrigin(size / 6.0f);
	// Setez pozitia glontului, astfel incat sa apara in mijloc jos
	m_bullet.setPosition(position);
	// Setez textura pentru glont
	m_bullet.setTexture(bulletTexture);

}

void Glont::Draw(sf::RenderWindow& window)
{
	window.draw(m_bullet);
}

void Glont::Fire(int speed)
{
	m_bullet.move(speed, 0);
}

void Glont::mv()
{
	m_bullet.move(0, -10);
}
void Glont::mv2()
{
	m_bullet.move(0, +10);
}
virtual void Glont::SetPos(const sf::Vector2f& v)
{

	m_bullet.setPosition(v);
}
