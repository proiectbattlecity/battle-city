#include "Score.h"


//Constructors
Score::Score()
	:m_score(0)
{
}


//Destructor
Score::~Score()
{
}


//Getters
uint16_t Score::GetScore() const
{
	return m_score;
}


//Setters
void Score::SetScore(const uint16_t& score)
{
	m_score = score;
}