#pragma once
#include <string>
class Tank
{
public:
	//Constructors
	Tank();
	Tank(const Tank& tank);
	Tank(Tank&& tank);
	Tank(int8_t lives, std::string color, float size, float moves, float positionOfX, float positionOfY);
	Tank* operator=(const Tank& other);

	//Destructor
	~Tank();

	//Getters
	int8_t GetLives() const;
	std::string GetColor() const;
	float GetSize() const;
	float GetMoves() const;
	float GetPositionOfX() const;
	float GetPositionOfY() const;

	//Setters
	void SetPositionOfX(const float& positionOfX);
	void SetPositionOfY(const float& positionOfY);

public:
	int8_t m_lives;
	std::string m_color;
	float m_size;
	float m_moves;
	float m_positionOfX;
	float m_positionOfY;
};