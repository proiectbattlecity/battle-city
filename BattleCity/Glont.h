#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
class Glont
{
public:
	Glont(sf::Texture* bulletTexture, sf::Vector2f size, sf::Vector2f playerPosition);
	void Draw(sf::RenderWindow& window);
	void Fire(int speed);
	void mv();
	void mv2();
	virtual void SetPos(const sf::Vector2f& v);

	sf::RectangleShape m_bullet;
};

