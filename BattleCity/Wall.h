#include<iostream>
#include"BrickWall.h"
#include"Collider.h"
#include"Projectile.h"
#include<vector>
#pragma once

class Wall {
public:

	//Constructors
	Wall();
	Wall(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position);
	Wall(uint32_t width, uint32_t height);
	Wall(const Wall& other);
	Wall& operator=(const Wall& other);

	Wall(Wall&& other) noexcept; // Move Constructor
	Wall& operator=(Wall&& other) noexcept; // Move = op

	//Destructor
	~Wall();

	//Getters
	float GetPositionOfX() const;
	float GetPositionOfY() const;
	Collider GetCollider();

	//Setters
	void SetWidth(const uint32_t& width);
	void SetHeight(const uint32_t& height);
	void SetPositionOfX(const float& x);
	void SetPositionOfY(const float& y);

	//Vector of objects
	Wall(int useless, sf::Vector2f position);

	//Methods
	void Draw(sf::RenderWindow& window);
	void DrawWallOfBricks(sf::RenderWindow& window);
	void CheckBulletCollision(Projectile bullet);

public:
	uint32_t m_width;
	uint32_t m_height;

	float m_positionOfX;
	float m_positionOfY;

	BrickWall** m_matrix;
	sf::RectangleShape m_wall;
	std::vector<BrickWall> m_bricks;

	//uint32_t mNumberOfWalls;
	//uint32_t **mMatrix;
};