#pragma once
#include <SFML/Graphics.hpp>
#include<SFML/Window.hpp>
#include<SFML/System.hpp>
#include <string>
#include <thread>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <sstream>
#include <locale>

class Timer {
public:

	//Methods
	void Create(sf::Text& t);
	void Display(sf::Text& t);

public:
	sf::Clock m_clock;
	sf::Time m_time;
	uint8_t m_countDown = 30;
	std::string m_countDownString = " ";
	sf::Text m_timerText;
};

