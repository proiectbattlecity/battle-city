#pragma once
#include "Block.h"

//Constructors
Block::Block() :
	m_positionOfX(0),
	m_positionOfY(0),
	m_height(0),
	m_width(0),
	m_collision(false)
{
}

Block::Block(float positionOfX, float positionOfY, uint32_t height, uint32_t width) :
	m_positionOfX(positionOfX),
	m_positionOfY(positionOfY),
	m_height(height),
	m_width(width),
	m_collision(false)
{
}

Block::Block(const Block& other)
{
	this->m_positionOfX = other.m_positionOfX;
	this->m_positionOfY = other.m_positionOfY;
	this->m_height = other.m_height;
	this->m_width = other.m_width;
	this->m_collision = other.m_collision;
}


Block::Block(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position)
{
	m_block.setSize(size);
	m_block.setTexture(texture);
	m_block.setPosition(position);
	m_block.setOrigin(size / 2.0f);
}


Block::~Block()
{

}

//Getters
float Block::GetPositionOfX() const
{
	return m_positionOfX;
}

float Block::GetPositionOfY() const
{
	return m_positionOfY;
}

uint32_t Block::GetHeight() const
{
	return m_height;
}

uint32_t Block::GetWidth() const
{
	return m_width;
}

bool Block::GetCollision() const
{
	return m_collision;
}


//Setters
void Block::SetWidth(const uint32_t& width)
{
	m_width = width;
}

void Block::SetHeight(const uint32_t& height)
{
	m_height = height;
}

void Block::SetPositionOfX(const float& positionOfX)
{
	m_positionOfX = positionOfX;
}

void Block::SetPositionOfY(const float& positionOfY)
{
	m_positionOfY = positionOfY;
}

void Block::SetCollision()
{
	m_collision = false;
}


//Methods
void Block::Draw(sf::RenderWindow& window)
{
	window.draw(m_block);
}

Collider Block::GetCollider()
{
	return Collider(m_block);
}
