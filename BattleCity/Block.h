#pragma once
#include<iostream>
#include <SFML/Graphics.hpp>
#include"Collider.h"

class Block
{
public:
	//Constructors
	Block();
	Block(float, float, uint32_t, uint32_t);
	Block(const Block& block);
	Block(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position);
	~Block();

	//Getters
	float GetPositionOfX() const;
	float GetPositionOfY() const;
	uint32_t GetHeight() const;
	uint32_t GetWidth() const;
	bool GetCollision() const;

	//Setters
	void SetWidth(const uint32_t& width);
	void SetHeight(const uint32_t& height);
	void SetPositionOfX(const float& positionOfX);
	void SetPositionOfY(const float& positionOfY);
	void SetCollision();

	//Methods
	void Draw(sf::RenderWindow& window);

	//Colliders
	Collider GetCollider();

public:
	float m_positionOfX;
	float m_positionOfY;
	bool m_collision;
	uint32_t m_height;
	uint32_t m_width;
	sf::RectangleShape m_block;
};

