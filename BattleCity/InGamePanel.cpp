#include "InGamePanel.h"

using namespace sf;

//Constructors
InGamePanel::InGamePanel(Player& player, EnemyTank& enemyTank, EnemyTank& enemyTank1, EnemyTank& enemyTank2, EnemyTank& enemyTank3, sf::RenderWindow& window)
{
	// Block texture
	sf::Texture blockTexture;
	if (!blockTexture.loadFromFile("blockTexture.png"))
	{
		std::cout << "The blockTexture does not load" << std::endl;
	}
	sf::Texture halfblockTexture;
	if (!halfblockTexture.loadFromFile("halfBlockTexture.png"))
	{
		std::cout << "The halfBlockTexture does not load" << std::endl;
	}
	sf::Texture birdTexture;
	if (!birdTexture.loadFromFile("cioara.png"))
	{
		std::cout << "The cioara does not load" << std::endl;
	}

	Block block(&blockTexture, sf::Vector2f(60, 50), sf::Vector2f(360, 185));
	Block block1(&halfblockTexture, sf::Vector2f(50, 25), sf::Vector2f(25, 373));
	Block block2(&halfblockTexture, sf::Vector2f(50, 25), sf::Vector2f(695, 373));
	Block block3(&birdTexture, sf::Vector2f(100, 100), sf::Vector2f(360, 660));

	// Wall texture
	sf::Texture wallTexture;
	if (!wallTexture.loadFromFile("wallTecture.png"))
	{
		std::cout << "The wallTecture does not load" << std::endl;
	}

	sf::Texture halfwallTexture;
	if (!halfwallTexture.loadFromFile("halfWallTexture.png"))
	{
		std::cout << "The halfWallTexture does not load" << std::endl;
	}

	/*Wall wall(&wallTexture, Vector2f(50, 225), Vector2f(60, 60));
	Wall wall1(&wallTexture, Vector2f(50, 225), Vector2f(170, 60));
	Wall wall2(&wallTexture, Vector2f(50, 175), Vector2f(280, 60));
	Wall wall3(&wallTexture, Vector2f(50, 175), Vector2f(390, 60));
	Wall wall4(&wallTexture, Vector2f(50, 225), Vector2f(500, 60));
	Wall wall5(&wallTexture, Vector2f(50, 225), Vector2f(610, 60));
	Wall wall6(&wallTexture, Vector2f(110, 50), Vector2f(110, 345));
	Wall wall7(&wallTexture, Vector2f(50, 50), Vector2f(280, 295));
	Wall wall8(&wallTexture, Vector2f(50, 50), Vector2f(390, 295));
	Wall wall9(&wallTexture, Vector2f(110, 50), Vector2f(500, 345));
	Wall wall10(&wallTexture, Vector2f(50, 175), Vector2f(60, 455));
	Wall wall11(&wallTexture, Vector2f(50, 175), Vector2f(170, 455));
	Wall wall12(&wallTexture, Vector2f(50, 150), Vector2f(280, 405));
	Wall wall13(&wallTexture, Vector2f(60, 50), Vector2f(330, 430));
	Wall wall14(&wallTexture, Vector2f(50, 150), Vector2f(390, 405));
	Wall wall15(&wallTexture, Vector2f(50, 175), Vector2f(500, 455));
	Wall wall16(&wallTexture, Vector2f(50, 175), Vector2f(610, 455));*/

	// Walls
	Wall walla(&wallTexture, Vector2f(50, 50), Vector2f(85, 85));
	Wall wallb(&wallTexture, Vector2f(50, 50), Vector2f(85, 135));
	Wall wallc(&wallTexture, Vector2f(50, 50), Vector2f(85, 185));
	Wall walld(&wallTexture, Vector2f(50, 50), Vector2f(85, 235));
	Wall walle(&halfwallTexture, Vector2f(50, 25), Vector2f(85, 273));

	Wall wall1a(&wallTexture, Vector2f(50, 50), Vector2f(195, 85));
	Wall wall1b(&wallTexture, Vector2f(50, 50), Vector2f(195, 135));
	Wall wall1c(&wallTexture, Vector2f(50, 50), Vector2f(195, 185));
	Wall wall1d(&wallTexture, Vector2f(50, 50), Vector2f(195, 235));
	Wall wall1e(&halfwallTexture, Vector2f(50, 25), Vector2f(195, 273));

	Wall wall2a(&wallTexture, Vector2f(50, 50), Vector2f(305, 85));
	Wall wall2b(&wallTexture, Vector2f(50, 50), Vector2f(305, 135));
	Wall wall2c(&wallTexture, Vector2f(50, 50), Vector2f(305, 185));
	Wall wall2d(&halfwallTexture, Vector2f(50, 25), Vector2f(305, 223));

	Wall wall3a(&wallTexture, Vector2f(50, 50), Vector2f(415, 85));
	Wall wall3b(&wallTexture, Vector2f(50, 50), Vector2f(415, 135));
	Wall wall3c(&wallTexture, Vector2f(50, 50), Vector2f(415, 185));
	Wall wall3d(&halfwallTexture, Vector2f(50, 25), Vector2f(415, 223));

	Wall wall4a(&wallTexture, Vector2f(50, 50), Vector2f(525, 85));
	Wall wall4b(&wallTexture, Vector2f(50, 50), Vector2f(525, 135));
	Wall wall4c(&wallTexture, Vector2f(50, 50), Vector2f(525, 185));
	Wall wall4d(&wallTexture, Vector2f(50, 50), Vector2f(525, 235));
	Wall wall4e(&halfwallTexture, Vector2f(50, 25), Vector2f(525, 273));

	Wall wall5a(&wallTexture, Vector2f(50, 50), Vector2f(635, 85));
	Wall wall5b(&wallTexture, Vector2f(50, 50), Vector2f(635, 135));
	Wall wall5c(&wallTexture, Vector2f(50, 50), Vector2f(635, 185));
	Wall wall5d(&wallTexture, Vector2f(50, 50), Vector2f(635, 235));
	Wall wall5e(&halfwallTexture, Vector2f(50, 25), Vector2f(635, 273));

	Wall wall6a(&wallTexture, Vector2f(50, 50), Vector2f(135, 370));
	Wall wall6b(&wallTexture, Vector2f(50, 50), Vector2f(185, 370));

	Wall wall7(&wallTexture, Vector2f(50, 50), Vector2f(305, 320));

	Wall wall8(&wallTexture, Vector2f(50, 50), Vector2f(415, 320));

	Wall wall9a(&wallTexture, Vector2f(50, 50), Vector2f(525, 370));
	Wall wall9b(&wallTexture, Vector2f(50, 50), Vector2f(575, 370));

	Wall wall10a(&wallTexture, Vector2f(50, 50), Vector2f(85, 480));
	Wall wall10b(&wallTexture, Vector2f(50, 50), Vector2f(85, 530));
	Wall wall10c(&wallTexture, Vector2f(50, 50), Vector2f(85, 580));
	Wall wall10d(&halfwallTexture, Vector2f(50, 25), Vector2f(85, 618));

	Wall wall11a(&wallTexture, Vector2f(50, 50), Vector2f(195, 480));
	Wall wall11b(&wallTexture, Vector2f(50, 50), Vector2f(195, 530));
	Wall wall11c(&wallTexture, Vector2f(50, 50), Vector2f(195, 580));
	Wall wall11d(&halfwallTexture, Vector2f(50, 25), Vector2f(195, 618));

	Wall wall12a(&wallTexture, Vector2f(50, 50), Vector2f(305, 430));
	Wall wall12b(&wallTexture, Vector2f(50, 50), Vector2f(305, 480));
	Wall wall12c(&wallTexture, Vector2f(50, 50), Vector2f(305, 530));

	Wall wall13(&wallTexture, Vector2f(60, 50), Vector2f(360, 455));

	Wall wall14a(&wallTexture, Vector2f(50, 50), Vector2f(415, 430));
	Wall wall14b(&wallTexture, Vector2f(50, 50), Vector2f(415, 480));
	Wall wall14c(&wallTexture, Vector2f(50, 50), Vector2f(415, 530));

	Wall wall15a(&wallTexture, Vector2f(50, 50), Vector2f(525, 480));
	Wall wall15b(&wallTexture, Vector2f(50, 50), Vector2f(525, 530));
	Wall wall15c(&wallTexture, Vector2f(50, 50), Vector2f(525, 580));
	Wall wall15d(&halfwallTexture, Vector2f(50, 25), Vector2f(525, 618));

	Wall wall16a(&wallTexture, Vector2f(50, 50), Vector2f(635, 480));
	Wall wall16b(&wallTexture, Vector2f(50, 50), Vector2f(635, 530));
	Wall wall16c(&wallTexture, Vector2f(50, 50), Vector2f(635, 580));
	Wall wall16d(&halfwallTexture, Vector2f(50, 25), Vector2f(635, 618));

	/*wallVec.push_back(walla);
	wallVec.push_back(wallb);
	wallVec.push_back(wallc);
	wallVec.push_back(walld);
	wallVec.push_back(walle);
	wallVec.push_back(wall1b);
	wallVec.push_back(wall1c);
	wallVec.push_back(wall1d);
	wallVec.push_back(wall1e);
	wallVec.push_back(wall2a);
	wallVec.push_back(wall2b);
	wallVec.push_back(wall2c);
	wallVec.push_back(wall2d);
	wallVec.push_back(wall3a);
	wallVec.push_back(wall3b);
	wallVec.push_back(wall3c);
	wallVec.push_back(wall3d);
	wallVec.push_back(wall4b);
	wallVec.push_back(wall4c);
	wallVec.push_back(wall4d);
	wallVec.push_back(wall4e);
	wallVec.push_back(wall5a);
	wallVec.push_back(wall5b);
	wallVec.push_back(wall5c);
	wallVec.push_back(wall5d);
	wallVec.push_back(wall5e);
	wallVec.push_back(wall6b);
	wallVec.push_back(wall7);
	wallVec.push_back(wall8);
	wallVec.push_back(wall9a);
	wallVec.push_back(wall9b);
	wallVec.push_back(wall10a);
	wallVec.push_back(wall10b);
	wallVec.push_back(wall10c);
	wallVec.push_back(wall10d);
	wallVec.push_back(wall11a);
	wallVec.push_back(wall11b);
	wallVec.push_back(wall11c);
	wallVec.push_back(wall11d);
	wallVec.push_back(wall12a);
	wallVec.push_back(wall12b);
	wallVec.push_back(wall12c);
	wallVec.push_back(wall13);
	wallVec.push_back(wall14a);
	wallVec.push_back(wall14b);
	wallVec.push_back(wall14c);
	wallVec.push_back(wall15a);
	wallVec.push_back(wall15b);
	wallVec.push_back(wall15c);
	wallVec.push_back(wall15d);
	wallVec.push_back(wall16a);
	wallVec.push_back(wall16b);
	wallVec.push_back(wall16c);
	wallVec.push_back(wall16d);*/

	// Collision with wall
	walla.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	walla.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	walla.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	walla.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	walla.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wallb.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wallb.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wallb.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wallb.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wallb.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wallc.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wallc.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wallc.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wallc.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wallc.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	walld.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	walld.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	walld.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	walld.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	walld.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	walle.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	walle.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	walle.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	walle.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	walle.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall1a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall1a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall1a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall1a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall1a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall1b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall1b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall1b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall1b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall1b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall1c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall1c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall1c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall1c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall1c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall1d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall1d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall1d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall1d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall1d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall1e.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall1e.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall1e.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall1e.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall1e.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall2a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall2a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall2a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall2a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall2a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall2b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall2b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall2b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall2b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall2b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall2c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall2c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall2c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall2c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall2c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall2d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall2d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall2d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall2d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall2d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall3a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall3a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall3a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall3a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall3a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall3b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall3b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall3b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall3b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall3b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall3c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall3c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall3c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall3c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall3c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall3d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall3d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall3d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall3d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall3d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall4a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall4a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall4a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall4a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall4a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall4b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall4b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall4b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall4b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall4b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall4c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall4c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall4c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall4c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall4c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall4d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall4d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall4d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall4d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall4d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall4e.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall4e.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall4e.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall4e.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall4e.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall5a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall5a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall5a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall5a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall5a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall5b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall5b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall5b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall5b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall5b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall5c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall5c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall5c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall5c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall5c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall5d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall5d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall5d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall5d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall5d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall5e.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall5e.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall5e.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall5e.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall5e.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall6a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall6a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall6a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall6a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall6a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall6b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall6b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall6b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall6b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall6b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall7.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall7.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall7.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall7.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall7.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall8.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall8.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall8.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall8.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall8.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall9a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall9a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall9a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall9a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall9a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall9b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall9b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall9b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall9b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall9b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall10a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall10a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall10a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall10a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall10a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall10b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall10b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall10b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall10b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall10b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall10c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall10c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall10c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall10c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall10c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall10d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall10d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall10d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall10d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall10d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall11a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall11a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall11a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall11a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall11a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall11b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall11b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall11b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall11b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall11b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall11c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall11c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall11c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall11c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall11c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall11d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall11d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall11d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall11d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall11d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall12a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall12a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall12a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall12a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall12a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall12b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall12b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall12b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall12b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall12b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall12c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall12c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall12c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall12c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall12c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall13.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall13.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall13.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall13.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall13.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall14a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall14a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall14a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall14a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall14a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall14b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall14b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall14b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall14b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall14b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall14c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall14c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall14c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall14c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall14c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall15a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall15a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall15a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall15a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall15a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall15b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall15b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall15b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall15b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall15b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall15c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall15c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall15c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall15c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall15c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall15d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall15d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall15d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall15d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall15d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	wall16a.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall16a.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall16a.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall16a.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall16a.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall16b.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall16b.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall16b.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall16b.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall16b.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall16c.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall16c.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall16c.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall16c.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall16c.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	wall16d.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	wall16d.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	wall16d.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	wall16d.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	wall16d.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	// Collision with blocks
	block.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	block.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	block.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	block.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	block.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	block1.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	block1.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	block1.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	block1.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	block1.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	block2.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	block2.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	block2.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	block2.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	block2.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);
	block3.GetCollider().CheckCollision(player.GetCollider(), 1.0f);
	block3.GetCollider().CheckCollision(enemyTank.GetCollider(), 1.0f);
	block3.GetCollider().CheckCollision(enemyTank1.GetCollider(), 1.0f);
	block3.GetCollider().CheckCollision(enemyTank2.GetCollider(), 1.0f);
	block3.GetCollider().CheckCollision(enemyTank3.GetCollider(), 1.0f);

	// Draw the block and the walls
	block.Draw(window);
	block1.Draw(window);
	block2.Draw(window);
	block3.Draw(window);
	walla.Draw(window);
	wallb.Draw(window);
	wallc.Draw(window);
	walld.Draw(window);
	walle.Draw(window);
	wall1a.Draw(window);
	wall1b.Draw(window);
	wall1c.Draw(window);
	wall1d.Draw(window);
	wall1e.Draw(window);
	wall2a.Draw(window);
	wall2b.Draw(window);
	wall2c.Draw(window);
	wall2d.Draw(window);
	wall3a.Draw(window);
	wall3b.Draw(window);
	wall3c.Draw(window);
	wall3d.Draw(window);
	wall4a.Draw(window);
	wall4b.Draw(window);
	wall4c.Draw(window);
	wall4d.Draw(window);
	wall4e.Draw(window);
	wall5a.Draw(window);
	wall5b.Draw(window);
	wall5c.Draw(window);
	wall5d.Draw(window);
	wall5e.Draw(window);
	wall6a.Draw(window);
	wall6b.Draw(window);
	wall7.Draw(window);
	wall8.Draw(window);
	wall9a.Draw(window);
	wall9b.Draw(window);
	wall10a.Draw(window);
	wall10b.Draw(window);
	wall10c.Draw(window);
	wall10d.Draw(window);
	wall11a.Draw(window);
	wall11b.Draw(window);
	wall11c.Draw(window);
	wall11d.Draw(window);
	wall12a.Draw(window);
	wall12b.Draw(window);
	wall12c.Draw(window);
	wall13.Draw(window);
	wall14a.Draw(window);
	wall14b.Draw(window);
	wall14c.Draw(window);
	wall15a.Draw(window);
	wall15b.Draw(window);
	wall15c.Draw(window);
	wall15d.Draw(window);
	wall16a.Draw(window);
	wall16b.Draw(window);
	wall16c.Draw(window);
	wall16d.Draw(window);
}


// Destructor
InGamePanel::~InGamePanel()
{
}
