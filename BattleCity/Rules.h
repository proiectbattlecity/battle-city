#pragma once
#include <SFML/Graphics.hpp>
#include <fstream>
#include <string>

class Rules
{
public:
	// Constructor
	Rules();

	// Destructor
	~Rules();

	// Methods
	void PrintFirstStep();
	void PrintSecondStep();
	void End();
	void Win();

	// Scoring
	void ScoreUP();
	void ScoreDOWN();

public:
	uint16_t m_score = 0;
	uint16_t m_hightScore = 0;

	std::string m_title;
	std::string m_rules;
};