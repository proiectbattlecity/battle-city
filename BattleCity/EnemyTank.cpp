#include "EnemyTank.h"
#include "Wall.h"
#include <time.h>
#include <stdlib.h>  
#include <cstdlib>

int generateRandom(int32_t max) {
	int randomNumber = rand();
	float random = (randomNumber % max) + 1;
	int myRandom = random;
	return random;
}


//Constructors
EnemyTank::EnemyTank(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position)
{
	mEnemyTank.setTexture(texture);
	mEnemyTank.setSize(size);
	mEnemyTank.setOrigin(size / 2.0f);
	mEnemyTank.setPosition(position);
}

EnemyTank::~EnemyTank()
{
	//this->mEnemyTank.~RectangleShape();
}


//Getters
sf::Vector2f EnemyTank::GetPosition()
{
	return mEnemyTank.getPosition();
}

Collider EnemyTank::GetCollider()
{
	return Collider(mEnemyTank);
}


//Setters
void EnemyTank::SetPosition(const sf::Vector2f& newPosition)
{
	mEnemyTank.setPosition(newPosition);
}

void EnemyTank::SetTexture(sf::Texture* texture)
{
	mEnemyTank.setTexture(texture);
}

void EnemyTank::SetFillColor(sf::Color color)
{
	mEnemyTank.setFillColor(color);
}


//Methods
void EnemyTank::Move(const sf::Vector2f& dir)
{
	mEnemyTank.move(dir);
}

void EnemyTank::Draw(sf::RenderWindow& window)
{
	window.draw(mEnemyTank);
}

void EnemyTank::Boundaries(unsigned windowWidth, unsigned windowHeight)
{
	//collision with the screen
	float enemyTankHalfSize = mEnemyTank.getSize().x / 2.0f;
	//Left collision
	if (mEnemyTank.getPosition().x < enemyTankHalfSize)
		mEnemyTank.setPosition(enemyTankHalfSize, mEnemyTank.getPosition().y);
	//Top collision
	if (mEnemyTank.getPosition().y < enemyTankHalfSize)
		mEnemyTank.setPosition(mEnemyTank.getPosition().x, enemyTankHalfSize);
	//Right collision
	if (mEnemyTank.getPosition().x + mEnemyTank.getGlobalBounds().width > windowWidth + enemyTankHalfSize)
		mEnemyTank.setPosition(windowWidth + enemyTankHalfSize - mEnemyTank.getGlobalBounds().width, mEnemyTank.getPosition().y);
	//Bottom collision
	if (mEnemyTank.getPosition().y + mEnemyTank.getGlobalBounds().height > windowHeight + enemyTankHalfSize)
		mEnemyTank.setPosition(mEnemyTank.getPosition().x, windowHeight + enemyTankHalfSize - mEnemyTank.getGlobalBounds().height);
}

void EnemyTank::Shoot(sf::RenderWindow& window)
{
	if (direction > 0 && direction < 5)
	{
		sf::Texture bulletTexture;
		if (!bulletTexture.loadFromFile("BulletUp.png"))
		{
			std::cout << "The BulletUp does not load" << std::endl;
		}
		Projectile newBullet(&bulletTexture, sf::Vector2f(100, 100), sf::Vector2f(mEnemyTank.getPosition().x + mEnemyTank.getSize().x / 4 - 13, mEnemyTank.getPosition().y + mEnemyTank.getSize().x / 4 - 5));
		newBullet.SetRotation(mEnemyTank.getRotation() + 90.0f);
		newBullet.direction = direction;
		newBullet.update();
		newBullet.Draw(window);
	}

	counter++;
	if (counter >= movementLenght)
	{
		direction = generateRandom(20);
		counter = 0;
	}
}

void EnemyTank::Movement()
{
	if (direction == 1)
	{
		mEnemyTank.move(0, -movementSpeed); //Up
		mEnemyTank.setRotation(0.f);
	}
	else if (direction == 2)
	{
		mEnemyTank.move(0, movementSpeed); //Down
		mEnemyTank.setRotation(180.f);
	}
	else if (direction == 3)
	{
		mEnemyTank.move(-movementSpeed, 0); //Left
		mEnemyTank.setRotation(270.f);
	}
	else if (direction == 4)
	{
		mEnemyTank.move(movementSpeed, 0); //Right
		mEnemyTank.setRotation(90.f);
	}
	else
	{
		//No movement
	}
	counter++;
	if (counter >= movementLenght)
	{
		direction = generateRandom(5);
		counter = 0;
	}

}


/*void EnemyTank::Movement1(float dt)
{

	const float moveSpeed = 1.0f;
	sf::Vector2f velocity;
	velocity.x = 0.1f;
	velocity.y = 0.1f;
	mEnemyTank.setRotation(-90.f);


	//velocity.x += +moveSpeed * dt;
	velocity.x += -moveSpeed;
	mEnemyTank.move(velocity);

	if (EnemyTank::CheckCollider() == true)
	{
		//velocity.y += moveSpeed * dt;
		velocity.y += moveSpeed;
		mEnemyTank.setRotation(180.f);
		mEnemyTank.move(velocity);
		while (EnemyTank::CheckCollider() == false)
		{
			//velocity.y += moveSpeed * dt;
			velocity.y += moveSpeed;
			mEnemyTank.move(velocity);
		}
	}

}

void EnemyTank::Movement2(float dt)
{

	const float moveSpeed = 1.5f;
	sf::Vector2f velocity;

	velocity.x = 0.1f;
	velocity.y = 0.1f;
	mEnemyTank.setRotation(90.f);

	//velocity.x += +moveSpeed * dt;
	velocity.x += moveSpeed;
	mEnemyTank.move(velocity);

	if (EnemyTank::CheckCollider() == true)
	{
		//velocity.y += moveSpeed * dt;
		velocity.y += moveSpeed;
		mEnemyTank.setRotation(180.f);
		mEnemyTank.move(velocity);
		while (EnemyTank::CheckCollider() == false)
		{
			//velocity.y += moveSpeed * dt;
			velocity.y += moveSpeed;
			mEnemyTank.move(velocity);
		}
	}


}

void EnemyTank::Movement3(float dt)
{

	const float moveSpeed = 2.5f;
	sf::Vector2f velocity;

	velocity.x = 0.1f;
	velocity.y = 0.1f;

	mEnemyTank.setRotation(180.f);
	/*velocity.x += +moveSpeed * dt;
velocity.y += moveSpeed;
mEnemyTank.move(velocity);

if (EnemyTank::CheckCollider() == true)
{
	//velocity.y += moveSpeed * dt;
	velocity.x += moveSpeed;
	mEnemyTank.setRotation(90.f);
	mEnemyTank.move(velocity);
	while (EnemyTank::CheckCollider() == false)
	{
		//velocity.y += moveSpeed * dt;
		velocity.y += moveSpeed;
		mEnemyTank.move(velocity);
	}
}
}*/


void EnemyTank::CheckBulletCollision(Projectile bullet)
{
	/*if (mEnemyTank.getGlobalBounds().intersects(bullet.m_bullet.getGlobalBounds()))
	{
		mEnemyTank.setPosition(sf::Vector2f(4234432, 4234423));
		mEnemyTank.setTexture(NULL);
		mEnemyTank.setFillColor(sf::Color::Transparent);
	}*/

	collision = 0;

	if (bullet.GetRight() > mEnemyTank.getPosition().x&& bullet.GetTop() < mEnemyTank.getPosition().y + mEnemyTank.getSize().y && bullet.GetBottom() > mEnemyTank.getPosition().y)
	{
		/*bullet.SetPosition(sf::Vector2f(4234432, 4234423));
		bullet.SetTexture(NULL);
		bullet.SetFillColor(sf::Color::Transparent);*/
		mEnemyTank.setPosition(sf::Vector2f(4234432, 4234423));
		mEnemyTank.setTexture(NULL);
		mEnemyTank.setFillColor(sf::Color::Transparent);
		collision = 1;
	}
}

bool EnemyTank::CheckCollider()
{
	//collision with the screen
	float enemyTankHalfSize = mEnemyTank.getSize().x / 2.0f;
	const unsigned windowWidth = 725;
	const unsigned windowHeight = 725;

	//Left collision
	if (mEnemyTank.getPosition().x < enemyTankHalfSize)
		return true;
	//Top collision
	if (mEnemyTank.getPosition().y < enemyTankHalfSize)
		return true;
	//Right collision
	if (mEnemyTank.getPosition().x + mEnemyTank.getGlobalBounds().width > windowWidth + enemyTankHalfSize)
		return true;
	//Bottom collision
	if (mEnemyTank.getPosition().y + mEnemyTank.getGlobalBounds().height > windowHeight + enemyTankHalfSize)
		return true;
	return false;
}

void EnemyTank::GenerateRandomEnemies(sf::RenderWindow& window)
{
	float dx = rand() % 725 + 1;
	float dy = rand() % 725 + 1;

	sf::Texture enemyTankTexture;
	if (!enemyTankTexture.loadFromFile("EnemyTank.png"))
	{
		std::cout << "The EnemyTank does not load" << std::endl;
	}

	EnemyTank enemy(&enemyTankTexture, sf::Vector2f(50, 50), sf::Vector2f(dx, dy));

	enemy.Draw(window);
}

