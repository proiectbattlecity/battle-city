#pragma once
#include<iostream>
#include <SFML/Graphics.hpp>
#include"Collider.h"

class BrickWall
{
public:
	//Constructors
	BrickWall(const sf::Vector2f& position);

	//Destructor
	~BrickWall();

	//Getters
	uint8_t GetDimension() const;
	float GetPositionOfX() const;
	float GetPositionOfY() const;

	//Setters
	void SetDimension(const uint8_t& dimension);
	void SetPositionOfX(const float& positionOfX);
	void SetPositionOfY(const float& positionOfY);

	//Methods
	void Draw(sf::RenderWindow& window);

public:
	float m_positionOfX;
	float m_positionOfY;
	uint8_t m_dimension;
	sf::RectangleShape m_brick;
};

