#include "WinPlayer.h"


//Constructors
WinPlayer::WinPlayer(Score score)
{
	this->m_score = score;
}


//Destructor
WinPlayer::~WinPlayer()
{
}


//Getters
Score WinPlayer::GetScore()
{
	return m_score;
}


//Methods
std::ostream& operator<<(std::ostream& out, const WinPlayer& win)
{
	out << "GAME WON! \n";
	out << "~ YOU ARE THE WINNER! ~\n";
	out << "SCORE PLAYER: " << win.m_score << "\n";
	out << "_____________________" << "\n";
	out << "TOTAL: " << win.m_score << "\n";
	return out;
}
