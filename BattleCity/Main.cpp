#pragma once
#include <string>
#include <SFML/Graphics.hpp>
#include<SFML/Window.hpp>
#include<SFML/System.hpp>
#include<SFML/Audio.hpp>
#include "Player.h"
#include "Tank.h"
#include "EnemyTank.h"
#include "Wall.h"
#include "Rules.h"
#include "Timer.h"
#include <iostream>
#include<fstream>
#include <vector>
#include<random>
#include "Block.h"
#include"Projectile.h"
#include "InGamePanel.h"
#include "../Logging/Logging/Logging.h"
using namespace sf;


int main()
{
	Rules rules;
	rules.PrintFirstStep();
	rules.PrintSecondStep();

	static const unsigned windowWidth = 725;
	static const unsigned windowHeight = 725;
	static const unsigned newWindowHeight = windowHeight - 5;

	// Create the main window
	sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "SFML works!");
	sf::Vector2i centerWindow((sf::VideoMode().width / 2) - 755, (sf::VideoMode::getDesktopMode().height / 2) - 390);

	// Load music 
	sf::Music music;
	if (!music.openFromFile("battle-city.flac"))
	{
		std::cout << "The music does not load" << std::endl;
	}
	music.play();

	sf::Music looseMusic;
	if (!looseMusic.openFromFile("loose.wav"))
	{
		std::cout << "The music does not load" << std::endl;
	}

	sf::Music winMusic;
	if (!winMusic.openFromFile("win.wav"))
	{
		std::cout << "The music does not load" << std::endl;
	}

	sf::Music bulletMusic;
	if (!bulletMusic.openFromFile("explosion.wav"))
	{
		std::cout << "The music does not load" << std::endl;
	}


	window.setFramerateLimit(120);
	auto dt = 0.02f;

	window.setKeyRepeatEnabled(true);

	// Init textures
	// Player
	sf::Texture playerTexture;
	if (!playerTexture.loadFromFile("TankTexture.png"))
		std::cout << "The TankTexture does not load" << std::endl;
	Player player(&playerTexture, Vector2f(50, 50), Vector2f(280, 690), 3);


	// Enemy tank
	sf::Texture enemyTankTexture;
	if (!enemyTankTexture.loadFromFile("EnemyTank.png"))
	{
		std::cout << "The EnemyTank does not load" << std::endl;
	}
	EnemyTank enemyTank(&enemyTankTexture, Vector2f(50, 50), Vector2f(25, 25));

	sf::Texture enemyTankTexture1;
	if (!enemyTankTexture1.loadFromFile("EnemyTank1.png"))
	{
		std::cout << "The EnemyTank1 does not load" << std::endl;
	}
	EnemyTank enemyTank1(&enemyTankTexture1, Vector2f(50, 50), Vector2f(700, 25));

	sf::Texture enemyTankTexture2;
	if (!enemyTankTexture2.loadFromFile("EnemyTank2.png"))
	{
		std::cout << "The EnemyTank2 does not load" << std::endl;
	}
	EnemyTank enemyTank2(&enemyTankTexture2, Vector2f(50, 50), Vector2f(25, 400));

	sf::Texture enemyTankTexture3;
	if (!enemyTankTexture3.loadFromFile("EnemyTank3.png"))
	{
		std::cout << "The EnemyTank3 does not load" << std::endl;
	}
	EnemyTank enemyTank3(&enemyTankTexture3, Vector2f(50, 50), Vector2f(700, 700));


	// Bullet
	sf::Texture bulletTexture;
	if (!bulletTexture.loadFromFile("BulletUp.png"))
	{
		std::cout << "The BulletUp does not load" << std::endl;
	}

	// Enemy tank vector
	std::vector<EnemyTank>::const_iterator iter2;
	std::vector<EnemyTank> enemyVec;
	enemyVec.push_back(enemyTank);
	enemyVec.push_back(enemyTank1);
	enemyVec.push_back(enemyTank2);
	enemyVec.push_back(enemyTank3);

	std::vector<Projectile>::const_iterator iter;
	std::vector<Projectile> bulletVec;
	bool isFiring = false;

	// Start the game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				music.stop();
				winMusic.play();
				window.close();
				rules.Win();
				rules.m_score = 0;
			}

			if (enemyTank.collision == 1 && enemyTank1.collision == 1 && enemyTank2.collision == 1 && enemyTank3.collision == 1)
			{
				music.stop();
				winMusic.play();
				window.close();
				rules.Win();
				rules.m_score = 0;
			}


			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				isFiring = true;
				bulletMusic.play();
			}
		}

		// Clear screen
		window.clear();

		if (isFiring == true)
		{
			Projectile newBullet(&bulletTexture, sf::Vector2f(100, 100), sf::Vector2f(player.GetPosition().x + player.m_player.getSize().x / 4 - 13, player.GetPosition().y + player.m_player.getSize().x / 4 - 5));
			newBullet.SetRotation(player.GetRotation() + 90.0f);
			newBullet.direction = player.direction;
			bulletVec.push_back(newBullet);

			isFiring = false;
		}

		//bulletVec[i].Shoot(player, bulletVec);
		InGamePanel panel(player, enemyTank, enemyTank1, enemyTank2, enemyTank3, window);
		/*enemyTank.Shoot(window);
		enemyTank1.Shoot(window);
		enemyTank2.Shoot(window);
		enemyTank3.Shoot(window);*/

		for (std::size_t i = 0; i < bulletVec.size(); ++i)
		{
			// Move
			bulletVec[i].update();
			bulletVec[i].Draw(window);

			// Out of window bounds
			if (bulletVec[i].GetPosition().x > window.getSize().x || bulletVec[i].GetPosition().y > window.getSize().y || bulletVec[i].GetPosition().x < 0 || bulletVec[i].GetPosition().y < 0)
				bulletVec.erase(bulletVec.begin() + i);


			// Enemy collision
			if (i < bulletVec.size())
			{
				enemyTank.CheckBulletCollision(bulletVec.at(i));
				enemyTank1.CheckBulletCollision(bulletVec.at(i));
				enemyTank2.CheckBulletCollision(bulletVec.at(i));
				enemyTank3.CheckBulletCollision(bulletVec.at(i));
			}

			// Collision with wall
			/*if (bulletVec.at(i).CheckCollider() == true)
			{
				bulletVec.erase(bulletVec.begin() + i);
			}*/

			// Destroy bullet when is collider with an enemy
			if (enemyTank.CheckCollider() == true)
			{
				bulletVec.erase(bulletVec.begin() + i);
				rules.ScoreUP();
			}

			if (enemyTank1.CheckCollider() == true)
			{
				bulletVec.erase(bulletVec.begin() + i);
				rules.ScoreUP();
			}

			if (enemyTank2.CheckCollider() == true)
			{
				bulletVec.erase(bulletVec.begin() + i);
				rules.ScoreUP();
			}

			if (enemyTank3.CheckCollider() == true)
			{
				bulletVec.erase(bulletVec.begin() + i);
				rules.ScoreUP();
			}

			/*if (bulletVec[i].m_bullet.getGlobalBounds().intersects(player.m_player.getGlobalBounds()))
			{
				player.m_lives--;
			}*/

		}
		uint32_t counter = 0;
		for (iter2 = enemyVec.begin(); iter2 != enemyVec.end(); iter2++)
		{
			/*enemyVec[e].Movement();
			enemyVec[e].Boundaries(windowWidth, windowHeight);
			enemyVec[e].Draw(window);*/
			if (enemyVec[counter].mEnemyTank.getGlobalBounds().intersects(player.m_player.getGlobalBounds()))
			{
				player.m_lives--;
			}
			counter++;
		}

		// Lives
		if (player.GetLives() == 0)
		{
			/*music.stop();
			looseMusic.play();
			window.close();
			rules.End();*/
		}

		// Movement
		player.Movement(dt);
		enemyTank.Movement();
		enemyTank1.Movement();
		enemyTank2.Movement();
		enemyTank3.Movement();

		// Collision with boundaries
		player.Boundaries(windowWidth, windowHeight);
		enemyTank.Boundaries(windowWidth, windowHeight);
		enemyTank1.Boundaries(windowWidth, windowHeight);
		enemyTank2.Boundaries(windowWidth, windowHeight);
		enemyTank3.Boundaries(windowWidth, windowHeight);

		// Draw the player
		player.Draw(window);

		// Draw the enemy
		enemyTank.Draw(window);
		enemyTank1.Draw(window);
		enemyTank2.Draw(window);
		enemyTank3.Draw(window);

		// Update the window
		window.display();

		// Clear screen
		window.clear();
	}

	return 0;
}