#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
class Bullet
{
public:
	//Constructors
	Bullet(sf::Texture* bulletTexture, sf::Vector2f size, sf::Vector2f playerPosition);
	Bullet(const Bullet& bullet);
	Bullet& operator=(Bullet&& other) noexcept;
	Bullet(Bullet&& other) noexcept;

	//Destuctor
	~Bullet();

	//Getters
	sf::Vector2f GetPosition()const;

	//Setters
	void setPos(const sf::Vector2f& newPos);

	//New Set of getters
	int16_t GetRight() const;
	int16_t GetLeft() const;
	int16_t GetTop() const;
	int16_t GetBottom() const;

	//Methods
	void Draw(sf::RenderWindow& window);
	void SetPosition(sf::Vector2f newPosition);
	void Fire(int speed);
	void move(sf::Vector2f dir);

public:
	sf::RectangleShape m_bullet;
};

