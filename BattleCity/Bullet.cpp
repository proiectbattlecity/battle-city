#include "Bullet.h"

//Constructors

Bullet::Bullet(sf::Texture* bulletTexture, sf::Vector2f size, sf::Vector2f position)
{
	// Setez dimensiunea glontului
	m_bullet.setSize(sf::Vector2f(25, 10));
	// Setez originea glontului in mijloc
	m_bullet.setOrigin(size / 2.0f);
	// Setez pozitia glontului, astfel incat sa apara in mijloc jos
	m_bullet.setPosition(position);
	// Setez textura pentru glont
	m_bullet.setTexture(bulletTexture);
}

Bullet::Bullet(const Bullet& other)
{
	//this->m_size = other.m_size;
	//this->m_moves = other.m_moves;
}

Bullet& Bullet::operator=(Bullet&& other) noexcept
{
	if (this == &other)
		return *this;
	this->~Bullet();
	new(this) Bullet
	{
		std::move(other)
	};
	return *this;
}

Bullet::Bullet(Bullet&& other) noexcept {
	//m_size = other.m_size;
	//m_moves = other.m_moves;
}


//Destructor
Bullet::~Bullet()
{
}


//Getters
sf::Vector2f Bullet::GetPosition() const
{
	return m_bullet.getPosition();
}


//Setters
void Bullet::setPos(const sf::Vector2f& newPos)  
{
	m_bullet.setPosition(newPos);
}


//New Set of getters
int16_t Bullet::GetRight() const
{
	return m_bullet.getPosition().x + m_bullet.getSize().x;
}

int16_t Bullet::GetLeft() const
{
	return m_bullet.getPosition().x;
}

int16_t Bullet::GetTop() const
{
	return m_bullet.getPosition().y;
}

int16_t Bullet::GetBottom() const
{
	return m_bullet.getPosition().y + m_bullet.getSize().y;
}


//Methods
void Bullet::Draw(sf::RenderWindow& window)
{
	window.draw(m_bullet);
}

void Bullet::SetPosition(sf::Vector2f newPosition) 
{
	m_bullet.setPosition(newPosition);
}

void Bullet::Fire(int speed)
{
	m_bullet.move(speed, 0);
}

void Bullet::move(sf::Vector2f dir)
{
	m_bullet.move(dir);
}
