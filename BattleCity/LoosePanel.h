#pragma once
#include"Score.h"

class LoosePanel
{
public:
	//Constructors
	LoosePanel(Score score);

	//Destructor
	~LoosePanel();

	//Getters
	Score GetScore() const;

	//Methods
	friend std::ostream& operator<<(std::ostream& out, const LoosePanel& loose);
	//void DisplayGameOver();

private:
	Score m_score;
};

