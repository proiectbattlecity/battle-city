#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include"Player.h"
//#include"Wall.h"
class Projectile
{
public:
	//Cosntructors
	Projectile(sf::Texture* bulletTexture, sf::Vector2f size, sf::Vector2f playerPosition);

	//Destructor
	~Projectile();

	//Methods
	void Draw(sf::RenderWindow& window);
	void update();
	void Fire(sf::Vector2f direction);
	//void Fire(sf::Vector2f speed, float angle);

	//Setters
	void SetRotation(const float& facing);
	void SetPosition(const sf::Vector2f& newPosition);

	//Getters
	sf::Vector2f GetPosition() const;
	bool CheckCollider();

	//New Set of getters
	int GetRight() const;
	int GetLeft() const;
	int GetTop() const;
	int GetBottom() const;

public:
	float movementSpeed = 3.f;
	uint8_t direction = 0; //1-up, 2-down, 3-left, 4-right
	sf::RectangleShape m_bullet;
};

