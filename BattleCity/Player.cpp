#include "Player.h"

//Constructors
Player::Player(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position, uint16_t lives)
{
	// Setez dimensiunea player-ului
	m_player.setSize(size);

	// Setez originea player-ului in mijloc
	m_player.setOrigin(size / 2.0f);

	// Setez pozitia player-ului, astfel incat sa apara in mijloc jos
	m_player.setPosition(position);

	// Setez textura pentru player
	m_player.setTexture(texture);

	m_lives = lives;
}


//Destructor
Player::~Player()
{
}


//Getters
sf::Vector2f Player::GetPosition() const
{
	return m_player.getPosition();
}


int Player::GetX() const
{
	return m_player.getPosition().x;
}

Collider Player::GetCollider()
{
	return Collider(m_player);
}

int Player::GetLives() const
{
	return m_lives;
}

float Player::GetRotation() const
{
	return m_player.getRotation();
}


//Methods
void Player::move(const sf::Vector2f& direction)
{
	m_player.move(direction);
}

void Player::Movement(float dt) {

	const float moveSpeed = 100.0f;
	sf::Vector2f velocity;

	//Player movement
	velocity.x = 0.f;
	velocity.y = 0.f;
	uint8_t ok = 1;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		m_player.setRotation(0.f);
		velocity.y += -moveSpeed * dt;
		direction = 1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		m_player.setRotation(180.f);
		velocity.y += moveSpeed * dt;
		direction = 2;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		m_player.setRotation(270.f);
		velocity.x += -moveSpeed * dt;
		direction = 3;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		m_player.setRotation(90.f);
		velocity.x += moveSpeed * dt;
		direction = 4;
	}
	m_player.move(velocity);
}

void Player::Boundaries(const unsigned windowWidth, const unsigned windowHeight)
{
	//Collision with the screen
	float playerHalfSize = m_player.getSize().x / 4.0f;

	//Left collision
	if (m_player.getPosition().x < playerHalfSize)
		m_player.setPosition(playerHalfSize, m_player.getPosition().y);

	//Top collision
	if (m_player.getPosition().y < playerHalfSize)
		m_player.setPosition(m_player.getPosition().x, playerHalfSize);

	//Right collision
	if (m_player.getPosition().x + m_player.getGlobalBounds().width > windowWidth + playerHalfSize)
		m_player.setPosition(windowWidth + playerHalfSize - m_player.getGlobalBounds().width, m_player.getPosition().y);

	//Bottom collision
	if (m_player.getPosition().y + m_player.getGlobalBounds().height > windowHeight + playerHalfSize)
		m_player.setPosition(m_player.getPosition().x, windowHeight + playerHalfSize - m_player.getGlobalBounds().height);
}

void Player::Shoot(sf::RenderWindow& window) {

	bool isFiring = false;
	std::vector<Bullet>bulletVec;
	sf::Texture bulletTexture;
	if (!bulletTexture.loadFromFile("BulletUp.png"))
	{
		std::cout << "The BulletUp does not load" << std::endl;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		isFiring = true;
	}

	if (isFiring == true)
	{
		Bullet newBullet(&bulletTexture, sf::Vector2f(100, 100), sf::Vector2f(0, 0));
		newBullet.setPos(sf::Vector2f(m_player.getPosition().x, m_player.getPosition().y));

		bulletVec.push_back(newBullet);
		isFiring = false;
	}

	for (int i = 0; i < bulletVec.size(); i++)
	{
		bulletVec[i].Draw(window);
		bulletVec[i].Fire(3);
	}
}

void Player::Draw(sf::RenderWindow& window)
{
	window.draw(m_player);
}