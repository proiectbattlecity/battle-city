#include "Details.h"

//Constructors
Details::Details()
	:m_score(0)
	, m_lives(0)
	, m_time()
{
}

Details::Details(const Timer& time, const uint32_t& score, const uint16_t& lives)
	: m_time(time)
	, m_score(score)
	, m_lives(lives)
{
}


//Destructor
Details::~Details()
{
}
