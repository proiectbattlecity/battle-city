#include "Projectile.h"

Projectile::Projectile(sf::Texture* bulletTexture, sf::Vector2f size, sf::Vector2f position)
{
	// Setez dimensiunea glontului
	m_bullet.setSize(sf::Vector2f(25, 10));

	// Setez pozitia glontului, astfel incat sa apara in mijloc jos
	m_bullet.setPosition(position);

	// Setez textura pentru glont
	m_bullet.setTexture(bulletTexture);
}


//Destructor
Projectile::~Projectile()
{
}


//Methods
void Projectile::Draw(sf::RenderWindow& window)
{
	window.draw(m_bullet);
}

void Projectile::Fire(sf::Vector2f direction)
{
	m_bullet.move(direction);
}

void Projectile::update()
{
	if (direction == 1)
		m_bullet.move(0, -movementSpeed); //Up
	if (direction == 2)
		m_bullet.move(0, movementSpeed); //Down
	if (direction == 3)
		m_bullet.move(-movementSpeed, 0); //Left
	if (direction == 4)
		m_bullet.move(movementSpeed, 0); //Right
}


//Setters
void Projectile::SetPosition(const sf::Vector2f& newPosition)
{
	m_bullet.setPosition(newPosition);
}

void Projectile::SetRotation(const float& facing) {
	m_bullet.setRotation(facing);
}


//Getters
sf::Vector2f Projectile::GetPosition() const
{
	return m_bullet.getPosition();
}

bool Projectile::CheckCollider()
{
	//collision with the screen
	float bulletHalfSize = m_bullet.getSize().x / 2.0f;
	const unsigned windowWidth = 800;
	const unsigned windowHeight = 800;

	//Left collision
	if (m_bullet.getPosition().x < bulletHalfSize)
		return true;
	//Top collision
	if (m_bullet.getPosition().y < bulletHalfSize)
		return true;
	//Right collision
	if (m_bullet.getPosition().x + m_bullet.getGlobalBounds().width > windowWidth + bulletHalfSize)
		return true;
	//Bottom collision
	if (m_bullet.getPosition().y + m_bullet.getGlobalBounds().height > windowHeight + bulletHalfSize)
		return true;
	return false;
}


//New Set of getters
int Projectile::GetRight() const
{
	return m_bullet.getPosition().x + m_bullet.getSize().x;
}

int Projectile::GetLeft() const
{
	return m_bullet.getPosition().x;
}

int Projectile::GetTop() const
{
	return m_bullet.getPosition().y;
}

int Projectile::GetBottom() const
{
	return m_bullet.getPosition().y + m_bullet.getSize().y;
}
