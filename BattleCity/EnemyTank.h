#include<SFML/Graphics.hpp>
#include"Collider.h"
#include"Bullet.h"
#include"Projectile.h"

#pragma once
class EnemyTank
{
public:
	// Constructors
	EnemyTank(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position);

	// Destructor
	~EnemyTank();

	// Getters
	sf::Vector2f GetPosition();
	Collider GetCollider();

	//Setters
	void SetPosition(const sf::Vector2f& newPosition);
	void SetTexture(sf::Texture* texture);
	void SetFillColor(sf::Color color);

	//Methods
	void Move(const sf::Vector2f& dir);

	void Draw(sf::RenderWindow& window);
	void Boundaries(unsigned windowWidth, unsigned windowHeight);
	void Shoot(sf::RenderWindow& window);
	void Movement();

	/*void Movement1(float dt);
	void Movement2(float dt);
	void Movement3(float dt);*/

	void CheckBulletCollision(Projectile bullet);
	bool CheckCollider();

	void GenerateRandomEnemies(sf::RenderWindow& window);

public:
	float movementSpeed = 2.f;
	static const uint16_t movementLenght = 100;
	uint8_t direction = 0; //1-up, 2-down, 3-left, 4-right
	uint32_t counter = 0;
	sf::RectangleShape mEnemyTank;
	int32_t collision = 0;

};

