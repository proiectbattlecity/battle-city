#pragma once
#include"Score.h"

class WinPlayer
{
	//Constructors
	WinPlayer(Score score);

	//Destructor
	~WinPlayer();

	//Getters
	Score GetScore();

	//Methods
	friend std::ostream& operator<<(std::ostream& out, const WinPlayer& win);

private:
	Score m_score;
};