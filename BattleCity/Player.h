#pragma once
#include <SFML/Graphics.hpp>
#include"Bullet.h"
#include"Collider.h"
#include "Glont.h"

class Player
{

public:
	//Constructors
	Player(sf::Texture* texture, sf::Vector2f size, sf::Vector2f position, uint16_t lives);

	//Destructor
	~Player();

	//Getters
	sf::Vector2f GetPosition() const;
	Collider GetCollider();
	int GetX() const;
	int GetLives() const;
	float GetRotation() const;

	//Methods
	void move(const sf::Vector2f& direction);
	void Movement(float dt);
	void Boundaries(unsigned windowWidth, unsigned windowHeight);
	void Shoot(sf::RenderWindow& window);
	void Draw(sf::RenderWindow& window);

public:
	sf::RectangleShape m_player;
	int m_lives;
	sf::Vector2f m_direction;
	uint8_t direction = 0; //1-up, 2-down, 3-left, 4-right
	//bool isShot = false;
};

