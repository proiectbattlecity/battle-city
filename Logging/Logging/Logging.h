#pragma once

#ifdef LOGGING_EXPORTS
#define LOGGER __declspec(dllexport)
#else
#define LOGGER __declspec(dllexport)
#endif
#include <chrono>
#include <iostream>
#include <string>

template<class ... Args>
void logi(Args&& ... params)
{
	//Logger::Log(Logger::Level::Info, std::forward<Args>(params)...);
}

class LOGGER Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};

public:
	template<class ... Args>
	static void Log(Level level, Args&& ... params)
	{
		std::ofstream outputFile;
		outputFile.open("../Logging/logging.txt", std::ios::out | std::ios::app);
		switch (level)
		{
		case Level::Info:
			outputFile << "[Info]";
			break;
		}
		((outputFile << ' ' << std::forward<Args>(params)), ...);
		outputFile << '\n';
		outputFile.close();
	}
public:
	Logger(std::ostream& os, Level minimumLevel = Level::Info);

	void log(const char * message, Level level);
	void log(const std::string& message, Level level);

	void setMinimumLogLevel(Level level);

public:
	std::ostream& os;
	Level minimumLevel;
};