#include "pch.h"
#include "CppUnitTest.h"
#include "Score.h"
#include "Block.h"
#include "Tank.h"
#include "BrickWall.h"
#include "Bullet.h"
#include "Player.h"
#include "Rules.h"
#include "Wall.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BattleCityTest
{
	TEST_CLASS(BattleCityTest)
	{
	public:

		TEST_METHOD(ScoreDefaultConstructor)
		{
			Score score;
			Assert::IsTrue(&Score::GetScore, 0);
		}

		TEST_METHOD(TankDefaultConstructor)
		{
			std::wstring name(L"NULL");
			const wchar_t* nameConv = name.c_str();

			Assert::IsTrue(&Tank::m_lives, NULL);
			Assert::IsTrue(&Tank::m_moves, NULL);
			Assert::IsTrue(&Tank::m_size, NULL);
			Assert::IsTrue(&Tank::m_color, nameConv);
			Assert::IsTrue(&Tank::m_positionOfX, NULL);
			Assert::IsTrue(&Tank::m_positionOfY, NULL);
		}

		TEST_METHOD(TankGetSize)
		{
			Tank tank(3, "red", 2.5, 10, 13.4, 13.4);
			Assert::IsTrue(tank.GetSize() == 2.5);
		}

		TEST_METHOD(TankGetLives)
		{
			Tank tank(3, "red", 2.5, 10, 13.4, 13.4);
			Assert::IsTrue(tank.GetLives() == 3);
		}

		TEST_METHOD(TankGetColor)
		{
			Tank tank(3, "red", 2.5, 10, 13.4, 13.4);
			Assert::IsTrue(tank.GetColor() == "red");
		}

		TEST_METHOD(TankGetMoves)
		{
			Tank tank(3, "red", 2.5, 10, 13.4, 13.4);
			Assert::IsTrue(tank.GetMoves() == 10);
		}

		TEST_METHOD(TankGetPositionOfX)
		{
			Tank tank(3, "red", 2.5, 10, 12, 13);
			Assert::IsTrue(tank.GetPositionOfX() == 12);
		}

		TEST_METHOD(TankGetPositionOfY)
		{
			Tank tank(3, "red", 2.5, 10, 13, 14);
			Assert::IsTrue(tank.GetPositionOfY() == 14);
		}


		TEST_METHOD(TankCopyConstructor)
		{
			Tank tank1(3, "red", 2.5, 10, 13, 14);
			Tank tank2 = tank1;

			Assert::IsTrue(tank1.GetLives() == tank2.GetLives());
			Assert::IsTrue(tank1.GetColor() == tank2.GetColor());
			Assert::IsTrue(tank1.GetMoves() == tank2.GetMoves());
			Assert::IsTrue(tank1.GetSize() == tank2.GetSize());
			Assert::IsTrue(tank1.GetPositionOfX() == tank2.GetPositionOfX());
			Assert::IsTrue(tank1.GetPositionOfY() == tank2.GetPositionOfY());
		}

		TEST_METHOD(TankOperatorEqual)
		{
			Tank tank1(3, "red", 2.5, 10, 13, 14);
			Tank tank2(tank1);

			Assert::IsTrue(tank1.GetLives() == tank2.GetLives());
			Assert::IsTrue(tank1.GetColor() == tank2.GetColor());
			Assert::IsTrue(tank1.GetMoves() == tank2.GetMoves());
			Assert::IsTrue(tank1.GetSize() == tank2.GetSize());
			Assert::IsTrue(tank1.GetPositionOfX() == tank2.GetPositionOfX());
			Assert::IsTrue(tank1.GetPositionOfY() == tank2.GetPositionOfY());
		}


		TEST_METHOD(TankConstructorWithParameters)
		{
			Tank tank(3, "red", 2.5, 10, 13, 14);

			Assert::IsTrue(tank.m_color == tank.GetColor());
			Assert::IsTrue(tank.m_lives == tank.GetLives());
			Assert::IsTrue(tank.m_moves == tank.GetMoves());
			Assert::IsTrue(tank.m_size == tank.GetSize());
			Assert::IsTrue(tank.m_positionOfX == tank.GetPositionOfX());
			Assert::IsTrue(tank.m_positionOfY == tank.GetPositionOfY());
		}


		TEST_METHOD(BrickWallConstructor)
		{
			Assert::IsTrue(&BrickWall::m_dimension, 0);
			Assert::IsTrue(&BrickWall::m_positionOfX, 0);
			Assert::IsTrue(&BrickWall::m_positionOfY, 0);
		}


		TEST_METHOD(BlockConstructor)
		{
			Assert::IsTrue(&Block::m_positionOfX, 0);
			Assert::IsTrue(&Block::m_positionOfY, 0);
			Assert::IsTrue(&Block::m_height, 0);
			Assert::IsTrue(&Block::m_width, 0);
			Assert::IsTrue(&Block::m_collision, false);
		}


		TEST_METHOD(WallConstructor)
		{
			Assert::IsTrue(&Wall::m_bricks, 0);
			Assert::IsTrue(&Wall::m_height, 0);
			Assert::IsTrue(&Wall::m_matrix, 0);
			Assert::IsTrue(&Wall::m_positionOfX, 0);
			Assert::IsTrue(&Wall::m_positionOfY, 0);
			Assert::IsTrue(&Wall::m_width, 0);
		}

		TEST_METHOD(RulesConstructor)
		{
			std::wstring name(L"Rules of the Game:\n");
			const wchar_t* nameTitle = name.c_str();

			std::wstring name2(L"\n\n1.Press W/Up Arrow if you want to go forward\n2.Press A/Left Arrow if you want to go Left\n3.Press D/Right Arrow if you want to go Right\n4.Press S/Down Arrow if you downward\n5.Press Space if you want to shoot\n \nPress Enter to start a new game!\n\n                GOOD LUCK!");
			const wchar_t* nameRules = name2.c_str();

			Assert::IsTrue(&Rules::m_score, 0);
			Assert::IsTrue(&Rules::m_hightScore, 0);
			Assert::IsTrue(&Rules::m_title, nameTitle);
			Assert::IsTrue(&Rules::m_rules, nameRules);
		}

		TEST_METHOD(ScoreConstructor)
		{
			Assert::IsTrue(&Score::m_score, 0);
		}

		TEST_METHOD(ProjectileConstructor)
		{
			Assert::IsTrue(&Projectile::movementSpeed, 0);
			Assert::IsTrue(&Projectile::direction, 0);
		}
	};
}
